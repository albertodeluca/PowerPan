# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PowerPanDockWidget
                                 A QGIS plugin
 This plugin pans around
                             -------------------
        begin                : 2017-10-16
        git sha              : $Format:%H$
        copyright            : (C) 2017 by -
        email                : -
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import pyqtSignal, QRegExp
from PyQt5.QtWidgets import QGridLayout, QPushButton, QFrame, QHBoxLayout, QLabel, QLineEdit, QSizePolicy, QSpacerItem
from PyQt5.QtGui import QRegExpValidator
from qgis.core import QgsPointXY

ss_black = 'QPushButton {color: black}'
ss_red = 'QPushButton {color: red}'
validator = '^(?:100|[1-9]?[0-9])$'

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'power_pan_dockwidget.ui'))


class PowerPanDockWidget(QtWidgets.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(PowerPanDockWidget, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect

        self.iface = iface
        self.setupUi(self)
        self.setWindowTitle('PowerPan 2.0')

        self.fra_main.setLineWidth(3)
        fra_main_lay = QGridLayout(self.fra_main)
        fra_main_lay.setContentsMargins(0, 0, 0, 0)
        fra_main_lay.setSpacing(0)

        self.btn_up = QPushButton(u'\u25B2')
        self.btn_up.setToolTip('Pan up')
        self.btn_down = QPushButton(u'\u25BC')
        self.btn_down.setToolTip('Pan down')
        self.btn_left = QPushButton(u'\u25C0')
        self.btn_left.setToolTip('Pan left')
        self.btn_right = QPushButton(u'\u25B6')
        self.btn_right.setToolTip('Pan rigth')

        self.fra_hoverlap = QFrame(self.fra_main)
        fra_hoverlap_lay = QHBoxLayout(self.fra_hoverlap)
        self.lbl_hoverlap = QLabel('H%:')
        self.txt_hoverlap = QLineEdit('10')
        self.txt_hoverlap.setToolTip('Horizontal overlap (%)')
        self.txt_hoverlap.setValidator(QRegExpValidator(QRegExp(validator)))
        self.txt_hoverlap.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)
        fra_hoverlap_lay.addWidget(self.lbl_hoverlap)
        fra_hoverlap_lay.addWidget(self.txt_hoverlap)

        self.fra_voverlap = QFrame(self.fra_main)
        fra_voverlap_lay = QHBoxLayout(self.fra_voverlap)
        self.lbl_voverlap = QLabel('V%:')
        self.txt_voverlap = QLineEdit('10')
        self.txt_voverlap.setToolTip('Vertical overlap (%)')
        self.txt_voverlap.setValidator(QRegExpValidator(QRegExp(validator)))
        self.txt_voverlap.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)
        fra_voverlap_lay.addWidget(self.lbl_voverlap)
        fra_voverlap_lay.addWidget(self.txt_voverlap)

        fra_main_lay.addWidget(self.fra_hoverlap, 0, 0)
        fra_main_lay.addWidget(self.btn_up, 0, 1)
        fra_main_lay.addWidget(self.fra_voverlap, 0, 2)
        fra_main_lay.addWidget(self.btn_down, 1, 1)
        fra_main_lay.addWidget(self.btn_left, 1, 0)
        fra_main_lay.addWidget(self.btn_right, 1, 2)

        fra_main_lay.addItem(QSpacerItem(0, 100, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self.setup()

    def setup(self):
        self.btn_up.pressed.connect(self.btn_up_pressed)
        self.btn_down.pressed.connect(self.btn_down_pressed)
        self.btn_left.pressed.connect(self.btn_left_pressed)
        self.btn_right.pressed.connect(self.btn_right_pressed)

    def btn_up_pressed(self):
        v_overlap = int(self.txt_voverlap.text())
        self.change_colors(True, False, False, False)
        extent = self.iface.mapCanvas().extent()
        new_y = extent.center().y() + (extent.height() * (100 - v_overlap)/100.)
        self.iface.mapCanvas().setCenter(QgsPointXY(extent.center().x(), new_y))
        self.iface.mapCanvas().refresh()

    def btn_down_pressed(self):
        v_overlap = int(self.txt_voverlap.text())
        self.change_colors(False, True, False, False)
        extent = self.iface.mapCanvas().extent()
        new_y = extent.center().y() - (extent.height() * (100 - v_overlap)/100.)
        self.iface.mapCanvas().setCenter(QgsPointXY(extent.center().x(), new_y))
        self.iface.mapCanvas().refresh()

    def btn_left_pressed(self):
        h_overlap = int(self.txt_hoverlap.text())
        self.change_colors(False, False, True, False)
        extent = self.iface.mapCanvas().extent()
        new_x = extent.center().x() - (extent.width() * (100 - h_overlap)/100.)
        self.iface.mapCanvas().setCenter(QgsPointXY(new_x, extent.center().y()))
        self.iface.mapCanvas().refresh()

    def btn_right_pressed(self):
        h_overlap = int(self.txt_hoverlap.text())
        self.change_colors(False, False, False, True)
        extent = self.iface.mapCanvas().extent()
        new_x = extent.center().x() + (extent.width() * (100 - h_overlap)/100.)
        self.iface.mapCanvas().setCenter(QgsPointXY(new_x, extent.center().y()))
        self.iface.mapCanvas().refresh()

    def change_colors(self, up, down, left, right):
        if up:
            self.btn_up.setStyleSheet(ss_red)
        else:
            self.btn_up.setStyleSheet(ss_black)

        if down:
            self.btn_down.setStyleSheet(ss_red)
        else:
            self.btn_down.setStyleSheet(ss_black)

        if left:
            self.btn_left.setStyleSheet(ss_red)
        else:
            self.btn_left.setStyleSheet(ss_black)

        if right:
            self.btn_right.setStyleSheet(ss_red)
        else:
            self.btn_right.setStyleSheet(ss_black)

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

